﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUnitaires {
    public class EAN13 {
        private int[] ean13;
        public EAN13(int[] ean13) {
            if(ean13.Length!=12) {
                throw new Exception("Un code Ean 13 doit être un tableau de 12 entiers");
            }
            this.ean13=new int[13];
            for(int i = 0;i<12;i++) {
                this.ean13[i]=ean13[i];
            }
        }
        public int PoidsPair() {
            int TotalPoidsPair = 0;
            for(int i = 1;i<=12;i=i+2) {
                TotalPoidsPair+=this.ean13[i];
            }
            return (TotalPoidsPair*3);
        }
        public int PoidsImpair() {
            int TotalPoidsImpair = 0;
            for(int i = 0;i<12;i=i+2) {
                TotalPoidsImpair+=this.ean13[i];
            }
            return TotalPoidsImpair;
        }
        public int Reste() {
            int LeReste = (this.PoidsImpair()+this.PoidsPair())%10;
            return LeReste;
        }
        public int Cle() {
            int LaClé = 10-Reste();
            return LaClé;
        }
        public override string ToString() {
            ean13[12]=Cle();
            string s = "";
            for(int i = 0;i<ean13.Length;i++) {
                s+=ean13[i];
                if(i==3||i==7||i==11) {
                    s+="-";
                }
            }
            return s;
        }
    }
}
