## Test unitaire

les test unitaire permette de tester une fonction � l'aide d'une valeur attendu, et d�terminer si cette fonction nous permet d'obtenir la valeur souhait�, afin de s'assurer du bon fonctionnement de cette fonction.

# Ma classe EAN13
Voici une image de la classe qui subiras des tests:

![image de la classe EAN13](images/EAN13.png)

# Classe EAN13Tests
La classe EAN13Test est charg� de v�rifier le bon fonctionement des m�thodes.
pour cela nous allons utiliser une nouvelle m�thode:

```cs
Assert.AreEqual(expected,actual);
```

cette m�thode permet de comparer la valeur que nous obtenons avec celle rechercher.

*Voici du code de la classe EAN13Tests*

![image de la classe EAN13Tests](images/EAN13Tests.png)

# Lancement du test
pour lancer le test nous devons aller dans la section test de visual studio et lancer tout les tests.

![image des tests rat�](images/testFailed.png)

*ici nous pouvons voir que certains tests sont rat�s nous devons r�parer nos erreurs de code et r�essayer*.

![image des tests r�ussi](images/testSucess.png)